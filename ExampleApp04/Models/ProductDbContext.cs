using Microsoft.EntityFrameworkCore;
namespace ExampleApp.Models
{
  public class ProductDbContext : DbContext
  {
    public ProductDbContext(DbContextOptions<ProductDbContext> options)
        : base(options)
    {
    }
    public DbSet<Product> Products { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      Console.WriteLine("Applying Migrations...");
      this.Database.Migrate();

      Console.WriteLine("Creating Seed Data...");
      modelBuilder.Entity<Product>().HasData(
        new Product(1, "Kayak", "Watersports", 275),
        new Product(2, "Lifejacket", "Watersports", 48.95m),
        new Product(3, "Soccer Ball", "Soccer", 19.50m),
        new Product(4, "Corner Flags", "Soccer", 34.95m),
        new Product(5, "Stadium", "Soccer", 79500),
        new Product(6, "Thinking Cap", "Chess", 16),
        new Product(7, "Unsteady Chair", "Chess", 29.95m),
        new Product(8, "Human Chess Board", "Chess", 75),
        new Product(9, "Bling-Bling King", "Chess", 1200)
      );
    }
  }
}