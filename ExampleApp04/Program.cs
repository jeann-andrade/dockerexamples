using ExampleApp.Models;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

IConfiguration configuration = new ConfigurationBuilder()
.SetBasePath(builder.Environment.ContentRootPath)
.AddEnvironmentVariables()
.Build();


var host = configuration["DBHOST"] ?? "localhost";
var port = configuration["DBPORT"] ?? "3306";
var user = configuration["DBUSER"] ?? "example-user";
var password = configuration["DBPASSWORD"] ?? "my_cool_secret";
var connectionString = $"server={host};Port={port};user={user};password={password};database=products";
var serverVersion = new MariaDbServerVersion(new Version(11, 1, 2));
// Add services to the container.

builder.Services.AddDbContext<ProductDbContext>(options =>
                options
                .UseMySql(connectionString, serverVersion)
                .LogTo(Console.WriteLine, LogLevel.Information)
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors());

builder.Services.AddSingleton<IConfiguration>(configuration);
builder.Services.AddTransient<IRepository, ProductRepository>();
builder.Services.AddControllersWithViews();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
