namespace ExampleApp.Models
{
    public class DummyRepository : IRepository
    {
        private static readonly Product[] DummyData = new Product[] {
            new() { Name = "Prod1",  Category = "Cat1", Price = 100 },
            new() { Name = "Prod2",  Category = "Cat1", Price = 100 },
            new() { Name = "Prod3",  Category = "Cat2", Price = 100 },
            new() { Name = "Prod4",  Category = "Cat2", Price = 100 },

        };

        public IQueryable<Product> Products => DummyData.AsQueryable();
    }
}